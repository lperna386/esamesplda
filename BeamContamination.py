# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 11:25:05 2023

@author: Leo

Sezione che mi serve ad affermare con certezza che il fascio è sporcato da componente adronica, molto probabilmente pioni. 
Lo facciamo vedere ricordando quale sia il setup sperimentale, in particolare prima dei calorimetri ci sono dei cristalli di 
4X0. Quindi mi aspetto che la componente adronica NON faccia sciame all'interno dei cristalli 
(L0 (lunghezza interazione adronica>X0 (lunghezza interazione elettronica))).
Operativamente quindi selezioniamo per esempio gli eventi nel picco e vediamo, per questi eventi, 
cosa succede all'interno dei cristalli (letti da SIPM). (Può essere utile guardare il setup sperimentale)

Usiamo i dati della run 169 (orientazione amorfa)

canali dei SIPM = 1-4

canali dei calo = 8-14

"""

#Potrebbe essere criticata la mia scelta di non salvare in memoria separatamente i bin e i conteggi di ogni istogramma. 
#tuttavia sono dell'idea che quello che so sicuramente che non mi servirà in futuro non lo salvo in memoria separatamente.
#%% Cose preliminari

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
from funzioni_utili import ImportData
from funzioni_utili import Gauss 

#%% Caricamento e rinominazione dati


data_169 =ImportData(169,'digi_ph',energy=120) #importo dati delle ph

ph_calos = np.sum(data_169['digi_ph'][:,8:15], 1) #sommo tutte le ph di tutti i calorimetri, sommo dalla colonna 8 alla 14 (canali digi 8-14), sommo per riga poichè riga = evento

ph_sipm = np.sum(data_169['digi_ph'][:,1:5],1)#sommo tutte le ph d tutti i sipms, sommo dalla colonna 1 alla 4 (canali digi 1-4), sommo per riga poichè riga = evento

#%% plotto gli istogrammi dei calo e sipm sommati 


fig, ax = plt.subplots(2)  # figura per isto 
fig.set_size_inches(15,10)
fig.subplots_adjust(hspace=.4)
ax = ax.flatten()

#calorimetri: 

h, bins = np.histogram(ph_calos, bins=800, range=[0, 20e3]) #isto del segnale integrato dei calorimetri 
binc = bins[:-1]+(bins[1]-bins[0])/2

energy = data_169['energy']

ax[0].plot(binc, h, ds='steps-mid', color='darkgreen', label=f'Calorimeter counts histogram (summed), \n energy ={energy} GeV ') 
ax[0].set_ylabel('Counts', fontsize=14)


# segno sul grafico il range del cut

start_calos = 10e3
end_calos = 11.5e3

ax[0].axvline(x=start_calos, ls='--', color='darkblue', label = 'range to fit')
ax[0].axvline(x=end_calos, ls='--', color='darkblue')

#fitto il picco dell'isto dei conteggi nel range sopra definito 

cond2fit = (binc>start_calos) & (binc<end_calos) #condizione per il fit, fitto la curva di conteggi dei calo solo nella finestra energetica sopra def.

p0 = [np.max(h), binc[np.argmax(h)], 500] #valori iniziali del fit

popt, pcov = curve_fit(Gauss, binc[cond2fit], h[cond2fit], p0 = p0, 
                               sigma=np.sqrt(h[cond2fit]),absolute_sigma=True)

ax[0].plot(binc[cond2fit], Gauss(binc[cond2fit], *popt), color='red',ls='--', label=f'Fitted gaussian peak in blue zone \n $\mu$ ={popt[1]:.4}, $\sigma$ ={popt[2]:.4}') 

#creo la mia condizione--> dichiaro fuori dal picco tutto quello che è a sinistra di 3 sigma dal picco.

cond_out_peak = ph_calos<(popt[1]-2*popt[2]) #condizione fuori picco

cond_in_peak =ph_calos>(popt[1]-2*popt[2])#condizione entro due sigma dal picco 

ax[0].axvline(x=popt[1]-2*popt[2], ls='--', color='black', label = '$x = \mu-2 \cdot \sigma$')

ax[0].grid()
ax[0].set_yscale('log')
ax[0].set_ylim(1,10e3) #sennò ho problemi col plot in log
ax[0].set_xlabel('Energy (ADC)', fontsize = 14)
ax[0].set_ylabel('Counts', fontsize = 14)

ax[0].legend(fontsize=12.5, loc ='upper right')

#SIPMs: 


h_sipms, bins = np.histogram(ph_sipm, bins=800, range=[0, 10e3]) #isto del segnale integrato dei sipms
binc_sipms = bins[:-1]+(bins[1]-bins[0])/2

ax[1].plot(binc_sipms, h_sipms, ds='steps-mid', color='darkgreen', label=f'SIPM s counts histogram (summed), \n energy ={energy} GeV') 
ax[1].set_xlabel('Energy (ADC)', fontsize = 14)
ax[1].set_ylabel('Counts', fontsize = 14)
ax[1].set_yscale('log')

ax[1].grid()

ax[1].legend(fontsize=12.5, loc ='upper right')


#%% Faccio l'istogramma dei sipms condizionato (con cond_out_peak) con sovrapposto l'istogramma precedente dei sipms

#isto dei sipm (con la condizione di essere a sinistra di 3 sigma dal picco principale dei calorimetri):

h_sipms_out_peak, bins = np.histogram(ph_sipm[cond_out_peak], bins=800, range=[0, 10e3]) 
binc_sipms_out_peak = bins[:-1]+(bins[1]-bins[0])/2


#isto dei sipm (con la condizione di essere a destra di 3 sigma dal picco principale dei calorimetri):

h_sipms_in_peak, bins = np.histogram(ph_sipm[cond_in_peak], bins=800, range=[0, 10e3]) 
binc_sipms_in_peak = bins[:-1]+(bins[1]-bins[0])/2

#plot:
fig, ax = plt.subplots()  
fig.set_size_inches(15,10)


ax.plot(binc_sipms, h_sipms, ds='steps-mid', color='darkgreen', label=f'SIPM s counts histogram, \n energy ={energy} GeV')

ax.plot(binc_sipms_out_peak, h_sipms_out_peak, ds='steps-mid', color='lime', label='SIPM s counts histogram with cuts (Energy< 2 $\sigma$ before the gaussian peak of calorimeters)')
 
ax.plot(binc_sipms_in_peak, h_sipms_in_peak, ds='steps-mid', color='cyan', label='SIPM s counts histogram with cuts (Energy> 2 $\sigma$ before the gaussian peak of calorimeters)')

ax.set_ylabel('Counts', fontsize=14)

ax.set_xlabel('Energy (ADC)', fontsize=14)  

ax.legend(fontsize=12.5, loc ='upper right')
ax.grid()
ax.set_yscale('log')


"""Considerazione fisica: abbiamo selezionato gli eventi che rilasciano meno energia nei calorimetri (2 sigma a sinistra del picco dei calorimetri) e abbiamo visto in corrispondenza di questi eventi cosa succede nei cristalli letti dai SIPMS (CURVA VERDE CHIARO).
Vediamo che in corrispondenza di questi eventi anche i SIPMS vedono poco (SEMPRE CURVA VERDE CHIARO). 
invece se seleziono gli eventi che rilasciano piu energia nel calorimetro vedo che anche nei cristalli, in corrispondenza di questi eventi, ho un grande rilascio energetico.
Questo ci fa subito dire con certezza che la coda a bassa energia degli spettri che tanto ci turbava è composta sicuramente da adroni perchè questi sciamano poco sia nei cristalli che nei calorimetri. Escludiamo il fatto che la coda 
di basse energie sia di elettroni (meno energetici) perchè se così fosse questi sciamerebbero tanto nei cristalli. Tuttavia la componente a bassa energia rilascia nei sipms un ordine di grandezza in meno di energia.
(ricorda che il deposito energetico per queste energie così elevate è pressocchè indipendente dall'energia (quindi dipendera solo dal materiale e dal TIPO di particella))"""


#%% CHECK : SE ABBIAMO FATTO LE COSE BENE, L'ISTOGRAMMA TOTALE DEVE ESSERE RECUPERATO
#SOMMANDO BIN PER BIN I CONTEGGI CON CONDIZIONE DENTRO PICCO + CONTEGGI CON CONDIZIONE DENTRO PICCO:


#plot:
fig, ax = plt.subplots()  
fig.set_size_inches(15,10)


ax.plot(binc_sipms, h_sipms, ds='steps-mid', color='darkgreen', label=f'SIPM s counts histogram, \n energy ={energy} GeV', alpha = 0.9)

ax.plot(binc_sipms, h_sipms, ds='steps-mid', color='red', label=f'SIPM s counts histogram, \n energy ={energy} GeV', alpha = 0.5)

ax.set_ylabel('Counts', fontsize=14)

ax.set_xlabel('Energy (ADC)', fontsize=14)  

ax.legend(fontsize=12.5, loc ='upper right')
ax.grid()
ax.set_yscale('log')

#i grafici si sovrappongono perfettamente!