# -*- coding: utf-8 -*-
"""
TEST BEAM H2 2022

Sezione forma e divergenza del fascio. Anche in funzione dell'energia.

Creato da Leonardo Perna 
il 18/06/2022

link to the logbook:
https://docs.google.com/document/d/1Qh8UEA80qhzt0GnI5E0zAQiObf8aXDfA4S_XmOAFaTI/edit


"""
#%%
"""COSE PRELIMINARI""" #importiamo pacchetti necessari 


import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit 
import numpy as np
from funzioni_utili import ImportData
from funzioni_utili import Gauss
from funzioni_utili import d




#%% CHECK RAPIDO PER VEDERE SE LE COSE HANNO SENSO:

#importo i dati. I dati sono compressi in formato HDF5 creati da un collega.

runs = np.array([144, 132, 126, 150, 151, 124]) #numeri di run in ordine energetico! 20-40-60-80-100-120 gev


xpos = {20+i*20:ImportData(run,"xpos")['xpos'] for i,run in enumerate(runs)}  #la chiave sarà data dall'energia (20+20*i), così mi dimentico del numero di run (sono smemorato)

#definiamo le posizioni per una run specifica, solo per dare un occhio al profilo del beam, scelgo quella ad energia maggiore--> 120 GeV

x1_120 = xpos[120][:,0] #posizione x del primo telescopio a 120 GeV
y1_120 = xpos[120][:,1] #posizione y del primo telescopio
x2_120 = xpos[120][:,2] #posizione x del secondo telescopio
y2_120 = xpos[120][:,3] #posizione y del secondo telescopio

#uso questo ordine perchè si evince dall'analisi successiva che le prime colonne si 
#riferiscono al telescopio più vicino al beam. (perchè ho fascio molto più pulito in forma e con la forma del trigger)
#questa cosa quindi la capiamo dopo (il fatto che per es x1 = xpos_120[:,0])


#%% CORRELAZIONI SPAZIALI
#adesso vediamo le correlazioni delle posizioni con isto 2d per capire se le coordinate
#sono coerenti (x con x e y con  y)

fig, ax= plt.subplots(2,1)
fig.set_size_inches(8,8)
fig.subplots_adjust(hspace=.2)  #per spaziare i grafici in altezza e farmi plottare bene le label
fig.suptitle("Correlation in position of two telescopes, 120 GeV.", fontsize = 20)

ax[0].hist2d(x1_120,x2_120, cmap = "jet", bins = 100, range=[[0,2],[0,2]]) #numero di bin è da intendersi sia su x che y
ax[1].hist2d(y1_120,y2_120, cmap = "jet", bins = 100, range=[[0,2],[0,2]])

ax[0].set_xlabel("x1 (cm)", fontsize = 15)
ax[0].set_ylabel("x2 (cm)", fontsize = 15)
ax[1].set_xlabel("y1 (cm)", fontsize = 15)
ax[1].set_ylabel("y2 (cm)", fontsize = 15)
#ax[0].set_box_aspect(1)
#ax[1].set_box_aspect(1)

"""vediamo che le pos sono correlate (e non anticorrelate)--> dato che sono correlate
non devo riflettere niente"""

#%% PROFILO DELLE TRACCE
"""
adesso procediamo con la visualizzazione degli isto 2d delle tracce lasciate dalle particelle.
vediamo che x1-y1 hanno esattamente la stessa forma del sistema di trigger, proprio come deve essere.
Per questo sono sicuro che esse siano le più vicine al fascio
"""

fig, ax=plt.subplots(2,1)    
fig.subplots_adjust(hspace=.2)
fig.set_size_inches(8,8)
fig.suptitle("Trace on T1 & T2, 120 GeV", fontsize=20)


ax[0].hist2d(x1_120,y1_120, cmap = "jet", bins =100,range=((0,2),(0,2)))
ax[1].hist2d(x2_120,y2_120, cmap = "jet", bins=100,range=((0,2),(0,2)))

ax[0].set_xlabel("$x_1$", fontsize=15)
ax[0].set_ylabel("$y_1$", fontsize=15)
ax[1].set_xlabel("$x_2$", fontsize=15)
ax[1].set_ylabel("$y_2$", fontsize=15)

plt.show()


#%% DIAMO UN OCCHIO AI PROFILI UNIDIMENSIONALI

#Guardiamo come sono fatti gli istogrammi 1d dei singoli profili:



name_pos = ["x1","y1","x2","y2"]

fig,ax=plt.subplots(2,2)
fig.set_size_inches(15,10)
fig.suptitle("Beam profile, 120GeV.", fontsize = 20)
fig.subplots_adjust(hspace = .4)
ax = ax.flatten() 
#definisco la lista di stringe utile per gli assi.
my_string=['Beam profile x1.','Beam profile y1.','Beam profile x2.', 'Beam profile y2.']


hx1, binsx1=np.histogram(x1_120,bins=100, range=[0,2])
hx2, binsx2=np.histogram(x2_120,bins=100, range=[0,2])
hy1, binsy1=np.histogram(y1_120,bins=100, range=[0,2])
hy2, binsy2=np.histogram(y2_120,bins=100, range=[0,2])

for i,a in enumerate(ax): #non salvo i dati all'interno di questo ciclo perchè questa è solo un analisi fast, per vedere se le cose hanno senso!
    
    h,bins=np.histogram(xpos[120][:,0+i],bins=100, range=[0,2])
    binc=bins[:-1]+(bins[1]-bins[0])/2
    a.plot(binc, h, ds = "steps-mid", c = "darkgreen", lw = 2, label = "Pulse height")
    a.set_xlabel("x1(cm)", fontsize=15)
    a.set_ylabel("Counts", fontsize=15)
    a.set_title( my_string[i])
    a.grid()
    a.legend(fontsize = 13, loc = 'upper left')
    
    #%% DIVERGENZA DEL FASCIO (fast look)

    #CALCOLO LA DISTANZA TRA DUE PUNTI DEI DUE TELESCOPI DIFFERENTI. RICORDIAMO CHE LE PRIME COORDINATE
    #SONO RIFERITE AL PRIMO TELESCOPIO (IL PRIMO CHE VIENE COLPITO DA FASCIO) E LE SECONDE RIFERITE AL SECONDO TELESCOPIO

    

    delta_x_120=xpos[120][:,2]-xpos[120][:,0] #x2° telescopio - x1°telescopio

    delta_y_120=xpos[120][:,3]-xpos[120][:,1] #y2° telescopio - y1°telescopio



    theta_x_120=np.arctan(delta_x_120/d)*1e6 #in micro radianti (murad)

    theta_y_120=np.arctan(delta_y_120/d)*1e6 #in micro radianti (murad)


    h_theta_x_120, bins_x_120=np.histogram(theta_x_120, bins=100, range=[-600,400])
    h_theta_y_120, bins_y_120=np.histogram(theta_y_120, bins=100, range=[-600,400])

    binc_x_120=bins_x_120[:-1]+(bins_x_120[1]-bins_x_120[0])/2
    binc_y_120=bins_y_120[:-1]+(bins_y_120[1]-bins_y_120[0])/2

    fig, ax=plt.subplots(2,1)
    fig.set_size_inches(15,10)
    fig.subplots_adjust(hspace=.6)


    ax[0].plot(binc_x_120, h_theta_x_120, ds="steps-mid", color="darkgreen", 
               lw=2, label = "Histogram of beam divergence")
    ax[0].set_xlabel("$\Theta$ ($\mu$rad)", fontsize=15)
    ax[0].set_ylabel("Counts", fontsize=12)


    ax[1].plot(binc_y_120, h_theta_y_120, ds="steps-mid", color="darkgreen", 
               lw=2, label = "Histogram of beam divergence")
    ax[1].set_xlabel("$\Theta$ ($\mu$rad)", fontsize=15)
    ax[1].set_ylabel("Counts", fontsize=12)

    #fitto con una gaussiana (non normalizzata)

    p0_x = [np.max(h_theta_x_120), binc_x_120[np.argmax(h_theta_x_120)], 50]
    p0_y = [np.max(h_theta_y_120), binc_x_120[np.argmax(h_theta_y_120)], 80]

    cond_x=(h_theta_x_120)>= (1/100)*np.max(h_theta_x_120) #mi costruisco la condizione per evitare che i punti che conteggiano zero pesino infinito.
    cond_y=(h_theta_y_120)>= (1/100)*np.max(h_theta_y_120)

    #fitto:
    popt_x_120, pcov_x_120 = curve_fit(Gauss, binc_x_120[cond_x], h_theta_x_120[cond_x], p0 = p0_x, 
                           sigma=np.sqrt(h_theta_x_120[cond_x]),absolute_sigma=True)

    popt_y_120, pcov_y_120 = curve_fit(Gauss, binc_y_120[cond_y], h_theta_y_120[cond_y], p0 = p0_y, 
                           sigma=np.sqrt(h_theta_y_120[cond_y]),absolute_sigma=True)

    print(f"Il parametro A_x è risultato essere {popt_x_120[0]} +- {np.sqrt(pcov_x_120[0,0])}")
    print(f"Il parametro mu_x è risultato essere {popt_x_120[1]} +- {np.sqrt(pcov_x_120[1,1])}")
    print(f"Il parametro sigma_x è risultato essere {popt_x_120[2]} +- {np.sqrt(pcov_x_120[2,2])}\n")

    print(f"Il parametro A_y è risultato essere {popt_y_120[0]} +- {np.sqrt(pcov_y_120[0,0])}")
    print(f"Il parametro mu_y è risultato essere {popt_y_120[1]} +- {np.sqrt(pcov_y_120[1,1])}")
    print(f"Il parametro sigma_y è risultato essere {popt_y_120[2]} +- {np.sqrt(pcov_y_120[2,2])}")

    ax[0].plot(binc_x_120, Gauss(binc_x_120 ,*popt_x_120), ls = "--", c = "k", lw = 3, 
               label = f"Fitted gaussian of x-divergence \n $\mu={popt_x_120[1]:.4} \pm {np.sqrt(pcov_x_120[1,1]):.4} \mu rad$ \n $\sigma={popt_x_120[2]:.4} \pm {np.sqrt(pcov_x_120[2,2]):.4} \mu rad$")

    ax[0].legend(fontsize = 14, loc = "upper right", title_fontproperties = {"size":16})

    ax[1].plot(binc_y_120, Gauss(binc_y_120 , *popt_y_120), ls = "--", c = "k", lw = 3, 
               label = f"Fitted gaussian of y-divergence \n $\mu={popt_y_120[1]:.4} \pm {np.sqrt(pcov_y_120[1,1]):.4} \mu rad$ \n $\sigma={popt_y_120[2]:.4} \pm {np.sqrt(pcov_y_120[2,2]):.4} \mu rad$  ")

    ax[1].legend(fontsize = 14, loc = "upper left", title_fontproperties = {"size":16})
    
    ax[0].grid()
    
    ax[1].grid()

#%% DIVERGENZA DEL FASCIO IN FUNZIONE DELL'ENERGIA. 

#ora quello che facciamo è esattamente la stessa cosa di sopra, per tutte le run, al variare dell'energia--> 10-40..120 GeV

energy = [20,40,60,80,100] #energia in gev su cui ciclo

#inizializzo un dizionario in cui salvare i valori estratti e gli errori del fit (chiave = energia)
   
#so che potrebbe sembrare inutile tenere traccia dell'energia e quindi di inizializzare un dizionario 
#tuttavia mi riservo in futuro, in base a come procederà l'analisi dati, di recuperare queste informazioni, caricando semplicemente i dizionari di mio interesse da questo modulo.
#insomma sono pignolo e ridondante per recuperare agevolmente queste informazioni in futuro SE serviranno.
 
value_x = {} #dizionario dei valori estratti per div in x

value_y = {} #dizionario dei valori estratti per div in y

err_x = {} # dizionario degli errori per divergenza in x

err_y = {} # dizionario degli errori per divergenza in y

for i, en in enumerate(energy): #ci fermiamo a 100GeV poiche per la run a 120 GeV il lavoro è gia stato fatto, è inutile rifarlo!
    
    
    delta_x=xpos[en][:,2]-xpos[en][:,0] #x2° telescopio - x1°telescopio

    delta_y=xpos[en][:,3]-xpos[en][:,1] #y2° telescopio - y1°telescopio



    theta_x=np.arctan(delta_x/d)*1e6 #in micro radianti (murad)

    theta_y=np.arctan(delta_y/d)*1e6 #in micro radianti (murad)


    h_theta_x, bins_x = np.histogram(theta_x, bins=100, range=[-600,400])
    h_theta_y, bins_y = np.histogram(theta_y, bins=100, range=[-600,400])

    binc_x=bins_x[:-1]+(bins_x[1]-bins_x[0])/2
    binc_y=bins_y[:-1]+(bins_y[1]-bins_y[0])/2
    
    #fitto con una gaussiana (non normalizzata)

    p0_x = [np.max(h_theta_x), binc_x[np.argmax(h_theta_x)], 50]
    p0_y = [np.max(h_theta_y), binc_x[np.argmax(h_theta_y)], 80]

    cond_x=(h_theta_x)>= (1/100)*np.max(h_theta_x) #mi costruisco la condizione per evitare che i punti che conteggiano zero pesino infinito.
    cond_y=(h_theta_y)>= (1/100)*np.max(h_theta_y)

    #fitto:
    popt_x, pcov_x = curve_fit(Gauss, binc_x[cond_x], h_theta_x[cond_x], p0 = p0_x, 
                           sigma=np.sqrt(h_theta_x[cond_x]),absolute_sigma=True)

    popt_y, pcov_y = curve_fit(Gauss, binc_y[cond_y], h_theta_y[cond_y], p0 = p0_y, 
                           sigma=np.sqrt(h_theta_y[cond_y]),absolute_sigma=True)

    #salvo i valori che mi servono per vedere divergenza in funz di energia
    #(salvo SOLO le info riguardanti la sigma della gaussiana (e l'errore su questo param.), perchè solo ciò è legato alla divergenza)
    
    value_x[en] = popt_x[2]

    value_y[en] = popt_y[2]
    
    err_x[en] = np.sqrt(pcov_x[2,2])
    
    err_y[en] = np.sqrt(pcov_x[2,2])
    
    
#inserisco per ultime, i valori calcolati precedentemente per la divergenza a 120 GeV


value_x[120] = popt_x_120[2]

value_y[120] = popt_y_120[2]
    
err_x[120] = np.sqrt(pcov_x_120[2,2])
    
err_y[120] = np.sqrt(pcov_x_120[2,2])

energy.append(120)


#plotto il tutto con gli errori

fig, ax=plt.subplots(2,1)
fig.set_size_inches(15,10)
fig.subplots_adjust(hspace=.6)

ax = ax.flatten()


ax[0].plot(energy, value_x.values() ,"*--", c = "orange" , label = "X-Beam's divergence")

ax[0].errorbar(energy, value_x.values(), err_x.values(), ls = 'none', label = 'Error from gaussian fits')

ax[0].set_xlabel('Energy (GeV)', fontsize = 15)

ax[0].set_ylabel('X-Divergence ($\mu rad$)', fontsize = 15)

ax[0].grid()

ax[0].legend(fontsize = 15)



ax[1].plot(energy, value_y.values() ,"*--", c = "orange" , label = "Y-Beam's divergence")

ax[1].errorbar(energy, value_y.values(), err_y.values(), ls = 'none', label = 'Error from gaussian fits')

ax[1].set_xlabel('Energy (GeV)', fontsize = 15)

ax[1].set_ylabel('Y-Divergence ($\mu rad$)', fontsize = 15)

ax[1].grid()

ax[1].legend(fontsize = 15)



