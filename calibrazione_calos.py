# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 17:21:27 2023

La calibrazione separata dei calorimetri non funziona, bisogna inventarsi qualcos'altro
La mia idea ora è quella di considerare i 7 calorimetri come uno solo.
Così facendo non avrò bisogno di ipotizzare che lo sciame sia contenuto nel singolo calorimetro

E' necessarioi però prima equalizzare i singoli calorimetri, altrimenti sarà incoerente sommare
tutti i segnali.

Prendo dunque delle run in cui ogni calorimetro viene colpito fascio. Queste run sono a 60Gev. 
Le uso per equalizzare la risposta mpponendo che la frazione di perdita energetica sia uguale
per ogni calorimetro singolo. 

(questi particolari sono difficili da spiegare per iscritto...)
@author: Leo
"""

#%%  cose preliminari 

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import json #per salvare i miei dizionari

from funzioni_utili import ImportData
from funzioni_utili import Gauss
from funzioni_utili import Line
from funzioni_utili import adc2en

runs = [140, 139, 142, 126, 127, 141, 138] 
#numero delle run a 60Gev di cui parlavo sopra.
#in queste run il fascio impatta su rispettivamente: LGBL(canale8digi) LGBR(canale9digi)
#LGCL(canale10digi) LGC(canale11digi) LGCR(canale12digi) LGTL(canale13digi) LGTR(canale14digi)

keys = ['LGBL','LGBR', 'LGCL', 'LGC', 'LGCR', 'LGTL', 'LGTR'] #chiavi del mio futuro dizionario sono nello stesso ordine del numero di run. Ovvero 
#per es la run 140 colpisce il calo LGBL, la run 139 il calo LGBR ecc

#Sono anche in ordine crescente di canale. Ovvero ricapitolando tutto : 
    
# run 140--> colpisce calo LGBL (principalmente)--> che è nel canale 8

# run 139--> colpisce calo LGBR (principalmente)--> che è nel canale 9

#ecc...
#%% Importiamo i dati necessari relativi alle ph

data_tot = {keys[i]: ImportData(runs[i],'digi_ph')['digi_ph'][:,8+i] for i in range(7)}

#%% TROVO I VALORI DI EQUALIZZAZIONE. RIFERISCO TUTTO AL CALORIMETRO CENTRALE. 

#il fattore di equalizzazione di LGC sarà =1, quello di LGBL sarà eta_LGBL = mu_LGBL/mu_LGC.
#quindi per trovare le ph equalizzate di LGBL dovrò calcolare Ph_LGBL *eta_LGBL 
#dove intendo mu --> il valore medio estratto dal fit gaussiano. 

fig, ax = plt.subplots(4,2)#creo un immagine per tutti gli isto fuori dal ciclo

ax = ax.flatten()

fig.set_size_inches(30,20)

fig.subplots_adjust(hspace=.4)

#definisco i range in cui istogrammare i miei dati (li azzecco perchè ho guardato tramite
#la funzione CutPh):

MyRange = {'LGBL':[1.5e3,5.5e3],'LGBR':[4.5e3,8.5e3],'LGCL':[6.5e3,10.5e3],'LGC':[4.5e3,8.5e3],
           'LGCR':[4.5e3,8.5e3],'LGTL':[5.5e3,9.5e3],'LGTR':[4.5e3,8.5e3]}

#inizializzo un dizionario vuoto per i valori di mu della gaussiana che mi servono a 
#equalizzare i calorimetri 

mu = {}

#inizializzo un dizionario vuoto per i valori degli errori su mu della gaussiana, non so se li userò in futuro  

err_mu = {}

for i, key in enumerate(keys):
            
    ph = data_tot[key]#definisco la variabile di pulse height
    
    #definisco una variabile per azzeccare automaticamente un range sensato 
    
    h, bins = np.histogram(ph, bins = 200, range =MyRange[key])#faccio l'istogramma
    
    binc = bins[:-1]+(bins[1]-bins[0])/2 #centro i bins
    
    #cerco di definire in maniera automatica una zona intorno al centro del picco dello spettro 
    #siccome è un intervallo asimmetrico rispetto al centro, dato che il beam è sporco ad energie 
    #basse devo fare delle condizioni asimmetriche 
    
    cond_destra = (binc>binc[np.argmax(h)]) & (h>0.1*np.max(h)) #condizione di essere a destra del picco & essere maggiore del 10 percento del massimo del picco. 
    
    destra = np.max(binc[cond_destra]) #trovo il valore della x (binc) della linea verticale di destra
    
    
    cond_sinistra = (binc<binc[np.argmax(h)]) & (h>0.70*np.max(h))#condizione di essere a sinistra del picco & essere maggiore  del 70 percento del massimo del picco.
    
    sinistra = np.min(binc[cond_sinistra]) #trovo il valore della x (binc) della linea verticale sinistra da plottare
    
    
    cond2fit = (binc>sinistra) & (binc<destra) #condizione del range per fittare
    
    
    ax[i].plot(binc, h, ds ='steps-mid', color ='darkgreen', 
               label = f'histograms of counts of \n {key}-Calorimeter') 
    
    ax[i].axvline(x = destra, ls ='--', color = 'darkblue', label = 'range to fit ')
    
    ax[i].axvline(x = sinistra,  ls ='--', color = 'darkblue')
    
    #ora facciamo il fit 
    
    p0 = [np.max(h), binc[np.argmax(h)], (destra-binc[np.argmax(h)])] #lo starting point della sigma è: x del limite destro-x del picco *2 è un po' rough ma almeno non devo inserirlo a mano
    
    popt, pcov = curve_fit(Gauss, binc[cond2fit], h[cond2fit], p0 = p0, 
                           sigma=np.sqrt(h[cond2fit]),absolute_sigma=True)
    
    ax[i].plot(binc[cond2fit], Gauss(binc[cond2fit], *popt), ls='--', 
               color = 'black', label = f'Fitted Gaussian $\mu$ = {popt[1]:.3} $\pm$ {np.sqrt(pcov[1,1]):.3}',lw = 4)
    
    ax[i].legend(fontsize = 15)
    
    ax[i].grid()
    
    mu[key] = popt[1]
    
    err_mu[key] = np.sqrt(pcov[1,1])
    
    
fig.delaxes(ax[7]) #cancello l'asse in più


#%% CALCOLO I FATTORI DI EQUALIZZAZIONE --> eta_mu e equalizzo (riscalo) le ph dividendo per questo fattore le ph


#metto tutto dentro un dizionario uso list compre

eta = {j: k/mu['LGC'] for j, k in mu.items()} #ciclo sulle chiavi e valori di mu.


#qui calcolo i valori delle ph equalizzate metto tutto, di nuovo, 
#all'interno' di un dizionario così posso ciclare agevolmente
    
PhEqua = {j: data_tot[j]/k for j,k in eta.items()}#ciclo sulle chiavi e valori di eta.

#%% Do un occhio agli spettri equalizzati per vedere se effettivamente
#la mia procedura ha senso..:
    
fig, ax = plt.subplots()

fig.set_size_inches(15,10)
    
for i,key in enumerate(keys):
    
    h, bins = np.histogram(PhEqua[key], bins = 100, range = (4e3,7.5e3), density = True) #li normalizzo al 
#proprio integrale perchè essendo dati con diverso numero di eventi non hanno, in corrispondenza
#dello stesso bin, lo stesso numero di conteggi. quindi uso density = True per normalizzare il tutto.
    
    binc = bins[:-1]+(bins[1]-bins[0])/2 #centro i bins
    
    ax.plot(binc, h, ds ='steps-mid', 
               label = f' {key}-Calorimeter equalized (and normalized)') 
    
    ax.legend(fontsize = 14, loc ='upper left')
    
    ax.set_xlabel('Energy (ADC)', fontsize = 15)
    
    ax.set_ylabel('Counts normalized', fontsize = 15)
    
    
    ax.grid()
    
#vediamo che la mia procedura di equalizzazione ha senso. 

#%% CALIBRAZIONE TOTALE CALORIMETRO  -- definizione dati 

#adesso quindi, dopo aver equalizzato, prendo una run (possibilmente ad alta energia--> 120GeV)

#e la uso per calibrare il calorimetro come blocco unico. Usando i singoli calorimetri EQUALIZZATI!!

#run di mio interesse rispettivamente a 120, 100, 80, 60, 40, 20 GeV

run = np.array([144, 132, 126, 150, 151, 124]) #run in ordine crescente di energia

energy = np.array([20,40,60,80,100,120]) #ordine energetico di energia

ph_cal_equa = {energy[i]: {keys[j]: ImportData(run[i], 'digi_ph')['digi_ph'][:,8+j]/eta[keys[j]] for j in range(7)} for i in range(6)} #creo un dizionario di dizionari. La prima chiave è l'energia della run in questione, la seconda chiave è il calorimetro del quale i dati vengono equalizzati e salvati.

ph_cal_tot = {energy[i]:sum(ph_cal_equa[energy[i]].values()) for i in range(6)} #sommo, evento per evento, gli array corrispondenti a tutti i calorimetri, per ogni energia possibile. (gia equalizzati ). NB sum(dic.values()) somma il primo elemento della prima chiave di dic col primo elemento della seconda chiave di dic ecc.. così per ogni chiave, per ogni elemento (il secondo, il terzo ecc..)



#%%ora, finalmente, possiamo calibrare il calorimetro.

fig, ax = plt.subplots(2,3)
fig.set_size_inches(35,25)
ax = ax.flatten()

popt_cal_calos =[] #inizializzo dizionario vuoto per inserire parametri estratti dal fit della calibrazione globale

err_cal_calos =[] #inizializzo dizionario vuoto per inserire errori estratti dal fit della calibrazione globale

for i, en in enumerate(energy):

    h, bins = np.histogram(ph_cal_tot[en], bins = 500, range = [1e3+i*1.5e3, 6e3+i*1.5e3])
    
    binc = bins[:-1]+(bins[1]-bins[0])/2
    
    
    ax[i].plot(binc, h, color = 'darkgreen', ds ='steps-mid', label =f'equalized and summed calorimeter spectrum, \n energy = {en}')
    
    ax[i].grid()
    
    #ciclo if perchè la run a 20GeV sembra patologica, con un picco inaspettato ad alte energie
    #quindi devo essere più stringente con il taglio a destra 
    
    if en == 20: 
        
        t = 0.2
        
    else: t = 0.05
    
    
    #cerco di definire in maniera automatica una zona intorno al centro del picco dello spettro 
    #siccome è un intervallo asimmetrico rispetto al centro, dato che il beam è sporco ad energie 
    #basse devo fare delle condizioni asimmetriche t è un parametro. Lo metto perchè non è facile 
    #azzeccare la finestra energetica giusta dello spettro ad energia = 20 GeV. Risulta avere molti conteggi 
    #anche ad energie elevate IDK why..
    
    cond_destra = (binc>binc[np.argmax(h)]) & (h>t*np.max(h)) #condizione di essere a destra del picco & essere maggiore del t*100 percento del massimo del picco. 
    
    destra = np.max(binc[cond_destra]) #trovo il valore della x (binc) della linea verticale di destra
    
    
    cond_sinistra = (binc<binc[np.argmax(h)]) & (h>0.60*np.max(h))#condizione di essere a sinistra del picco & essere maggiore  del 60 percento del massimo del picco.
    
    sinistra = np.min(binc[cond_sinistra]) #trovo il valore della x (binc) della linea verticale sinistra da plottare
    
    
    cond2fit = (binc>sinistra) & (binc<destra) #condizione del range per fittare
    
    
    ax[i].axvline(x = destra, ls ='--', color = 'darkblue', label ='range to fit')
    
    ax[i].axvline(x = sinistra,  ls ='--', color = 'darkblue')
    
    #ora facciamo il fit 
    
    p0 = [np.max(h), binc[np.argmax(h)], (destra-binc[np.argmax(h)])] #lo starting point della sigma è: x del limite destro-x del picco *2 è un po' rough ma almeno non devo inserirlo a mano
    
    popt, pcov = curve_fit(Gauss, binc[cond2fit], h[cond2fit], p0 = p0, 
                            sigma=np.sqrt(h[cond2fit]),absolute_sigma=True)
    
    ax[i].plot(binc[cond2fit], Gauss(binc[cond2fit], *popt), ls='--', 
                color = 'black', label = f'Fitted Gaussian $\mu$ = {popt[1]:.3} $\pm$ {np.sqrt(pcov[1,1]):.3} \n $\sigma$ = {popt[2]:.3} $\pm$ {np.sqrt(pcov[2,2]):.3}',lw = 4)
    
    ax[i].legend(fontsize = 13, loc ='upper left')
    
    popt_cal_calos.append( popt) #inserisco i parametri trovati nella mia variabile, sono in ordine ENERGETICO !
    
    err_cal_calos.append( np.sqrt(np.diag(pcov)))
    
popt_cal_calos = np.array(popt_cal_calos)#trasformo in un array per comodità (dovrò selezionare dei valori e fare il fit)

err_cal_calos = np.array(err_cal_calos)
    

#%% RETTA DI CALIBRAZIONE !


fig, ax = plt.subplots()  # figura per retta calibrazione
fig.set_size_inches(15,10)

p0 = [90, 140]

#popt_cal_calos[:,1] sarà la colonna dei valori estratti di mu della gaussiana!
#et err_cal_calos[:,1] sarà l'errore associato!

mu_cal_calos = popt_cal_calos[:,1]

err_mu_cal_calos = err_cal_calos[:,1]

popt_L, pcov_L = curve_fit(Line, energy , mu_cal_calos , p0 = p0)

err_L = np.sqrt(np.diag(pcov_L)) #errori sul fit della retta di calibrazione, mi servono per salvarli, alla fine del codice

ax.errorbar(energy, mu_cal_calos, yerr = err_mu_cal_calos, marker='s', color = 'blue', ls ='none', label = 'Error on data points', markersize=7) #plotto l'errorbar con errore = errore estratto dal fit del valor medio della gaussiana (mu)

ax.plot(energy, Line(energy, *popt_L), color ='black', ls='--', label = f'Calibration from linear fit \n m = {popt_L[0]:.4} $\pm$ {np.sqrt(pcov_L[0,0]):.4} ADC/GeV \n q = {popt_L[1]:.4} $\pm$ {np.sqrt(pcov_L[1,1]):.4} ADC ', lw = 3)  #plotto anche la retta con i valori estratti dal fit di cui appena sopra

ax.plot()

ax.grid()

ax.legend(fontsize = 15)

#%% Parametri di bontà del fit: residui --> distanza tra i punti del modello lineare
#e i punti sperimentali

ds = (mu_cal_calos-Line(energy, *popt_L))/Line(energy, *popt_L)*100 #distanza tra punti sperimentali e quelli previsti dal fit, normalizzati ai valori previsti dal fit, in percentuale

fig, ax = plt.subplots()

fig.set_size_inches(10,5)

ax.plot(energy, ds,'.', markersize = 6,color = 'red', label = 'residuals of linear fit calculated as \n $100 \cdot \dfrac{ADC_{exp}-ADC_{fit}}{ADC_{fit}}$ ')

ax.axhline(y = 0, ls ='--', color ='grey') 

ax.set_xlabel('Energy (GeV)', fontsize = 12)

ax.set_ylabel('Residuals (%)', fontsize = 12)

ax.legend(fontsize = 10)

ax.grid()

#abbiamo residui al massimo del 4%



#%% CHECK DI CONSISTENZA--> Vediamo come si sommano le ph (equalizzate) di queste run di calibrazione

fig, ax = plt.subplots()

fig.set_size_inches(20,11)

popt_res = [] #inizializzo una lista vuoto in cui metterò dentro i valori dei parametri estratti dal fit per calcolare dopo la risoluzione energetica R=100*sigma/mu

err_res = [] #inizializzo una lista vuoto in cui metterò dentro i valori di errore utili alla propagazione dell'errore della ris en. Salvo solo gli error su mu e sigma della gaussiana.


for i,en in enumerate(energy):
    
    gev = adc2en(ph_cal_tot[en], *popt_L)
    
    h, bins = np.histogram(gev, bins=100, range=[5+i*15,45+i*17],density = True) #normalizzo tutti i conteggi al proprio integrale almeno il fatto di normalizzarle al poprio integrale li rende piu o meno alti uguali
    
    binc = bins[:-1]+(bins[1]-bins[0])/2
    
    ax.plot(binc, h, ds ='steps-mid')
    
    #range2fit:
    
    #ciclo if perchè la run a 20GeV sembra patologica, con un picco inaspettato ad alte energie
    #quindi devo essere più stringente con il taglio a destra 
    
    if en == 20: 
        
        t = 0.2
        
    else: t = 0.05
    
    
    #cerco di definire in maniera automatica una zona intorno al centro del picco dello spettro 
    #siccome è un intervallo asimmetrico rispetto al centro, dato che il beam è sporco ad energie 
    #basse devo fare delle condizioni asimmetriche t è un parametro. Lo metto perchè non è facile 
    #azzeccare la finestra energetica giusta dello spettro ad energia = 20 GeV. Risulta avere molti conteggi 
    #anche ad energie elevate IDK why..
    
    cond_destra = (binc>binc[np.argmax(h)]) & (h>t*np.max(h)) #condizione di essere a destra del picco & essere maggiore del t*100 percento del massimo del picco. 
    
    destra = np.max(binc[cond_destra]) #trovo il valore della x (binc) della linea verticale di destra
    
    
    cond_sinistra = (binc<binc[np.argmax(h)]) & (h>0.60*np.max(h))#condizione di essere a sinistra del picco & essere maggiore  del 60 percento del massimo del picco.
    
    sinistra = np.min(binc[cond_sinistra]) #trovo il valore della x (binc) della linea verticale sinistra da plottare
    
    
    cond2fit = (binc>sinistra) & (binc<destra) #condizione del range per fittare
    
    #ora facciamo il fit 
    
    p0 = [np.max(h), binc[np.argmax(h)], (destra-binc[np.argmax(h)])] #lo starting point della sigma è: x del limite destro-x del picco *2 è un po' rough ma almeno non devo inserirlo a mano
    
    popt, pcov = curve_fit(Gauss, binc[cond2fit], h[cond2fit], p0 = p0, 
                            sigma=np.sqrt(h[cond2fit]),absolute_sigma=True)
    
    popt_res.append( popt[1:]) #inserisco valori dei parametri estratti dal fit (mu e sigma soltanto) (IN ORDINE ENERGETICO)
    
    err_res.append( np.sqrt(np.diag(pcov)[1:])) #appendo, per ogni energia, un array degli errori su mu e sigma rispettivamente (IN ORDINE ENERGETICO)
    
    ax.plot(binc[cond2fit], Gauss(binc[cond2fit], *popt), ls='--', 
                color = 'black', label = f'Fitted Gaussian $\mu$ = {popt[1]:.3} $\pm$ {np.sqrt(pcov[1,1]):.3} GeV \n (expected {en} GeV)',lw = 4)
    
    ax.legend(fontsize = 12, loc ='upper right')
    
#li metto una volta per tutte solo alla fine
ax.set_xlabel('Energy (GeV)', fontsize = 20)
ax.set_ylabel('Counts normalized', fontsize = 20)
ax.grid() 

#questo metodo sembra funzionare bene (a meno di qualche gev di discrepanza.) 
#Molto meglio della procedura che trovi nella cartella 
#calibrazioni_calos_separati!! (NELLA BRANCH TestCalibrazioneCalo!!!! )


#%% RISOLUZIONE ENERGETICA DEL CALORIMETRO, in funzione di energia: Scelgo le run definite sopra (run) poiche arrivano fino a 120 GeV e avrò piu punti per fare la ris en
    
#calcolo della risoluzione energetica R= 100*sigma_gev/mu_gev

res_en = [ 100*popt_res[i][1]/popt_res[i][0] for i in range(len(energy))]

#ADESSO ARRIVA LA QUESTIONE DELLA PROPAGAZIONE DELL'ERRORE .. :(
#prima di tutto R = 100 * sigma_gev/mu_gev --> delta sigma = np.sqrt((delta (sigma_gev)/mu_gev)^2 + (sigma_gev*delta(mu_gev)/(mu_gev)^2)^2)
#ricorda sia in err_res sia in popt_res ci sono al primo posto valore ed errore di mu. Al secondo errore e valore di sigma

sigma_res = [np.sqrt((err_res[i][1]/popt_res[i][0])**2+(popt_res[i][1]*err_res[i][0])/popt_res[i][0]**2 ) for i in range(len(energy))]

#fitto il tutto con il modello classico dei calorimetri--> vedi in funzioni utili


#plotto tutto : 
    
fig, ax = plt.subplots()

fig.set_size_inches(7,5)

ax.errorbar(energy, res_en, yerr = sigma_res, marker='s', color = 'blue', ls ='-.', label = 'Error on data points', markersize=2) #plotto l'errorbar con errore = errore estratto dal fit del valor medio della gaussiana (mu)

ax.set_xlabel('Energy',fontsize = 15)

ax.set_ylabel('Energetic resolution (%)',fontsize = 15)

ax.grid()


ax.legend(fontsize = 15)



#%% SALVATAGGIO DATI IMPORTANTI decido che salverò i parametri della retta di calibrazione
# in array format. Invece per i parametri di equalizzazione trovo più comodo salvarli 
#come dizionari :
    
#parametri calibrazione e relativi errori
np.savez('DATA/parametri_calibrazione_tot.npz', popt_L = popt_L, err_L = err_L)

#parametri di equalizzazione:

# Salvo il dizionario degli eta non penso mi serviranno gli errori relativi a questo
with open("DATA/eta.json", "w") as file:
    json.dump(eta, file)
    

    

