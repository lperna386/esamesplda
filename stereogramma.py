# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 00:05:25 2023

Sezione sullo stereogramma del cristallo--> grafico per trovare l'angolo corretto (angolo in cui il deposito energetico nei sipm è massimo)
Ora dobbiamo guardare i dati delle xinfo e delle ph. In particolare  vogliamo analizzare 
l'andamento delle ph in funzione degli angoli cradle and gonio Le info sull'angolo provengono dal goniometro
e sono all'interno delle prime due colonne di x info.
La sfida qui è capire come mettere in ordine questi angoli. Infatti devo isolare tutti gli eventi
che hanno in comune entrambi gli angoli (all'interno della stessa run)
e analizzare le ph relative. Per fare questo uso pandas (nella funzione unique_columns_with_indices)'
In particolare prima di tutto estraggo tutte le coppie di angoli senza ripetizione. Dopodichè metto in ordine
crescente tali angoli senza ripetizione in una matrice bidimensionale molto piu piccola(ordine prima secondo la colonna 1 poi secondo la colonna 2). Infine estraggo tutti 
gli indici di riga (evento !!) in cui tale coppia di angoli è presente.
In particolare la funzione restituisce due array. Matrice ordinata senza ripetizione (ordinata prima secondo la prima colonna e poi secondo la seconda colonna, per ogni evento.)
e array di indici (di riga, cioè di evento) in cui si trova tale coppia di angoli. E le cose sono coerenti. il primo set di indici riguarda la prima coppia di angoli restituiti.
gli angoli sono cradle e gonio (nomi tecnici)

Le run in cui sono stati fatti scan angolari sono: 110-116 100gev.

Dato che vogliamo vedere il deposito energetico nel cristallo guardiamo i canali dei sipm

(canali 1-4 del digi)

@author: Leo
"""
#%% COSE PRELIMINARI 



import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
from funzioni_utili import ImportData
from funzioni_utili import unique_columns_with_indices


#importo i dati degli scan angolari da 110 a 116:

ph = {i:np.sum(ImportData(i, 'digi_ph')['digi_ph'][:,1:5],axis=1)for i in range(110,117)} #importo i dati delle ph con la chiave = nrun. in particolare salvo nella mia variabile dizionario la somma delle ph evento per evento -> riga x riga dei canali 1-2-3-4 del digi (sipm)

ang_ind = {i:unique_columns_with_indices(ImportData(i, 'xinfo')['xinfo'][:,0:2])for i in range(110,117)}#salvo in questo dizionario la matrice bidimensionale di elementi non ripetuti e in ordine crescente, e gli indici in cui tali eventi si trovano. IL TUTTO IN UNA TUPLA!!! (legere documentazione della mia funzione)

#estraggo i valori degli angoli da ang_ind perchè mi servono dopo IN ORDINE ! SIA ANGOLARE (all'interno della SINGOLA RUN), SIA DI RUN!!

ang1 = [tupla[0][:,0] for tupla in ang_ind.values()] #questi non li metto in dizionari perchè mi serviranno per fare dei grafici, semplicemente. Le altre cose le metto nei dizionari perchè essendo tantissime informazioni mi fa sentire piu sicuro averle in un oggetto tanto ordinato come il dizionario, che posso aprire e consultare.

ang2 = [tupla[0][:,1] for tupla in ang_ind.values()]

for i in range(7) :
    assert len(ang_ind[i+110][0][:,0]) == len (ang_ind[i+110][1]), "DIMENSIONS ARE WRONG, CHECK THE ORDERING PROCEDURE :("  #riga di controllo, le righe della matrice di elementi (angoli) e gli elementi dell'array con all'interno gli indici devono avere stessa dimensione.

#%%
#per far capire al lettore--> discommenta

#print(f' la matrice di angoli ordinati, per run=110: {ang_ind[110][0]}\n gli indici relativi: {ang_ind[110][1]}')
                                                                                                              # chiave = nrun 
#%% ORA ANDIAMO AL SODO.. DEVO VEDERE IL DEPOSITO ENERGETICO IN FUNZIONE DEI
#DUE ANGOLI. PER CALCOLARE IL DEPOSITO ENERGETICO MEDIO DECIDO DI FARE UNA MEDIA DELLE PH
#BRUTALMENTE, PER OGNI VALORE DI COPPIE DI ANGOLI. PER FORTUNA ABBIAMO UNA LISTA DI LISTE DI INDICI
#CHE INDICANO LE RIGHE RELATIVE ALLE COPPIE DI ANGOLI (ang_ind[nrun][1]).

#decido di fare la media delle ph. Guarda sotto per spiegazioni!

#ph_angles = {i : np.array([np.sum(ph[i][ang_ind[i][1][j]]) for j in range(len(ang_ind[i][1]))]) for i in range(110,117)} #dizionario --> chiave = nrun. qui metto (in ordine di ang_ind[nrun][0], la coppia di angoli) i valori delle ph sommate (in un vettore in cui l'elemento j-simo corrisponde alla somma delle ph nella condizione di avere angoli pari alla j-sima riga di ang_ind[nrun][0]).

ph_angles = {i : np.array([np.mean(ph[i][ang_ind[i][1][j]]) for j in range(len(ang_ind[i][1]))]) for i in range(110,117)} #dizionario --> chiave = nrun. Qui dentro faccio questo: fisso la run, prendo le ph del numero di run ph[i], gli applico gli indici trovati sopra, relativi 

#agli angoli sopra ordinati (ricorda l'ordine degli indici [ang_ind[nrun][1] è coerente con le coppie di angoli che trovo in [ang_ind[nrun][0] ). In questi indici, quindi in una determinata configurazione di coppie di angoli, faccio la media delle PH (per valutare il deposito energetico!). Questo lo faccio per ogni configurazione di coppie di angoli. 
#dopodichè passo alla run successiva e faccio la stessa identica cosa. Così facendo metto tutto in dizionario. Dove il singolo elemento del dizionario è un array che ha le medie di ph, per ogni configurazione di coppia di angolo. Questo array ha ordine perfettamente coerente con la configurazione angolare che trovo in ang_ind[nrun][0]. 

#quindi per fare un esempio.. fisso il numero della run -> ph_angles[nrun] avrà un array con all'interno la media delle ph (della stessa run) con coerenza degli indici angolari. Quindi il primo 
#ph_angles[nrun][0] sarà la somma per tutti quegli eventi che hanno coppia di angoli ang_ind[nrun][0][0] (prima riga della matrice delle coppie di angoli)


#%% COSTRUISCO LO STEREOGRAMMA TOTALE : costruisco i vettori relativi alle ph sommate e agli angoli relativi 
    

E_w_tot = np.concatenate(list(ph_angles.values()))#concateno le ph, semplicemente concatenando i values di ph_angles.

ang1_tot = np.concatenate(ang1) #concateno tutti gli ang1. Sono coerenti con i valori di ph sommati !
ang2_tot = np.concatenate(ang2)#idem per ph2

#%% COSTRUISCO LO STEREOGRAMMA: 
    
#costruisco il tutto:
    
xpos = ang1_tot # x coordinata gonio
ypos = ang2_tot # y coordinata cradle

norm = colors.Normalize(E_w_tot.min(), E_w_tot.max())
colors1 = cm.jet(norm(E_w_tot)) #normalizzo la scala di colori della colorbar, ovvero assegniamo al valore più grande il colore più "acceso", al valore più piccolo il colore più "spento"
cmap = 'jet'

area = 10 #area pallini
fig, ax = plt.subplots(figsize=(12,10))
plt.scatter(xpos, ypos, s=area, c= colors1, cmap = cm.jet)
plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap))#creo la colorbar

ax.set_xlabel(r'$\theta_{gonio}$ $(\mu rad)$ ',fontsize=20)
ax.set_ylabel(r'$\theta_{cradle}$ $(\mu rad)$',fontsize=20)
ax.set_title('Mean PH (a.u) value vs gonio and cradle angle',fontsize=17)
ax.grid()

#da questo grafico iniziale e grossolano vediamo che dobbiamo concentrarci sulla zona in alto a destra!

#Quelli piu bassi sono quelli delle run 110 111 112 

#COME LO CAPIAMO ?? 
#FACILE DA QUI: 
for i in range(110,113):
    print(f'For run {i}, gonio angles are = {ang_ind[i][0][:,0]}') #infatti dal plot vediamo che il gonio è piccolo per le run in cui la somma delle ph è piccola, quindi quando sono fuori asse.

#ci concentriamo sulle run successive  da 114 a 116.

#Notare che il mastro-cristallaio, in sede di esperimento, ci ha detto quale fosse il suo 
#angolo d'asse! Quindi ho un importantissimo valore di riferimento per capire se ho fatto 
#le cose per bene. Nelle prossime run quindi plotterò anche l'angolo che l'addetto al cristallo ha comunicato.

#%% Stereogramma ristretto:--> creo le x, le y e i conteggi (somme di ph)

E_w_stretto = np.concatenate((ph_angles[114],ph_angles[115],ph_angles[116]))#concateno le ph, semplicemente concatenando i values di ph_angles.

ang1_stretto = np.concatenate(ang1[-3:]) #concateno tutti gli ang1 e ang2 delle run di mio interesse. Cioè delle ultime 3 run di mio interesse!

ang2_stretto = np.concatenate(ang2[-3:])

#%% STEREOGRAMMA RISTRETTO:

xpos = ang1_stretto # x coordinata gonio
ypos = ang2_stretto # y coordinata cradle

norm = colors.Normalize(E_w_stretto.min(), E_w_stretto.max())#normalizzo la colorbar, o meglio definisco come deve essere normalizzata
colors1 = cm.jet(norm(E_w_stretto)) #normalizzo la colorbar
cmap = 'jet'

area = 10 #area pallini
fig, ax = plt.subplots(figsize=(12,10))
plt.scatter(xpos, ypos, s=area, c= colors1,cmap = cm.jet)
plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap))

ax.set_xlabel(r'$\theta_{gonio}$ $(\mu rad)$ ',fontsize=20)
ax.set_ylabel(r'$\theta_{cradle}$ $(\mu rad)$',fontsize=20)
ax.set_title('Mean PH (a.u) value vs gonio and cradle angle',fontsize=17)

ax.plot(3139909, 67400, '*', c= 'black', markersize = 10,label='Correct angle') #questo è valore segnato in logbook
ax.plot(xpos[np.argmax(E_w_stretto)],ypos[np.argmax(E_w_stretto)], '*', c= 'red', markersize = 10,label='Angle founded by me') #questo è valore segnato in logbook

ax.legend(loc = 'upper left', fontsize = 15)
ax.grid()

#DA QUI VEDIAMO CHE LA RUN IN CUI SI TROVA IL MASSIMO RILASCIO DI PH (STELLA ROSSA)
#E' la 115 infatti lo capiamo perchè se plottiamo gli angoli gonio di tale run ottengo molti valori diversi:
    
print(f'For run 115, gonio angles are = {ang_ind[115][0][:,0]}')

#%% ANDAMENTO PH IN FUNZIONE DI  GONIO: Infine notiamo l'andamento del deposito energetico (media) 
#in funzione dell'angolo gonio proprio nella run 115, ovvero in quella in cui ho il MASSIMO
#deposito energetico. 

#DISCLAIMER: ad essere sincero questo grafico lo posso fare tranquillamente senza preoccuparmi dell'ordine solo perchè sto plottando ph media in funzione dell'angolo gonio. Ovvero del PRIMO angolo presente in xinfo,
#e quindi relativo alla prima colonna della matrice ang_ind[115][0]. Infatti se una persona scrupolosa pensa a come sono costruite le cose (in particolare il dizionario ang_ind e di conseguenza l'ordine di tutti i dati seguenti) 
#si rende conto che la funzione  unique_columns_with_indices
#mette in ordine prima rispetto alla prima colonna e poi rispetto la seconda colonna. Questo vuol dire che se ho la matrice di angoli del tipo:
#[[1,0],[0,1],[0,2],[1,1]] dopo l'applicazione della funzione avrò [[0,1],[0,2],[1,0],[1,1]] quindi la prima colonna sicuramente sarà in ordine crescente!
#Ma la seconda non per forza. Comunque il problema non si pone in questo momento, (non sarebbe nemmeno troppo difficile mettere in ordine crescente tale colonna e girare di conseguenza l'altro vettore che si vuol plottare. )--> e infatti nella cella seguente lo faccio!

fig, ax = plt.subplots()

fig.set_size_inches(7,7)

ax.plot(ang_ind[115][0][:,0], ph_angles[115],'--*', color = 'darkblue', label = f'SIPMs average PH as a function \n of the gonio angle\n $\Theta max$ = {ang_ind[115][0][:,0][np.argmax(ph_angles[115])]:.2} $\mu rad$ ' )        

ax.set_xlabel(r'$\theta_{gonio}$ ($\mu rad$)', fontsize = 18)

ax.set_ylabel('Mean energy (a.u)', fontsize = 18)

ax.grid()

ax.legend(fontsize = 13, loc = 'lower center')



#%% FACCIO LA STESSA COSA PER LA RUN 116, IN CUI VOGLIO VEDERE LA PH MEDIA IN FUNZIONE
#DELL'ANGOLO CRADLE. IN + fixo il problema di avere un ordinamento sbagliato (vedi sopra)

#Disclaimer questo metodo funziona bene perchè sono sicuro che in questa run ho uno scan angolare
#su cradle. Quindi sono pressocchè sicuro che lo stesso angolo cradle non si ripeta, quindi usare np.unique() può andare bene.
#se si dovesse ripetere lo stesso elemento all'interno di un array e uso np.unique possono succedere cose molto misleading--> 
#provare per credere 

#a,b = np.unique([2,2,1,1,3,3,1,1], return_index = True)#gli indici non sono esaustivi perchè l'elemento 1 si ripete.. Infatti se sostituisci gli ultimi 1,1 con 2,2 ottieni gi stessi indici (b). proprio per evitare questi problemi non ho usato np.unique()
#per evitare questo problema metto riga di controllo che mi dice se ci sono elementi ripetuti--> np.unique mi restituisce un array della stessa lunghezza di quello di partenza solo se non ci sono elementi ripetuti !!
#uso infatti unique e non sort per 1 avere piu controllo 2 avere un ritorno di indici

cradle = ang_ind[116][0][:,1] 


#li metto in ordine e ricavo gli indici di tale ordine

cradle_ordered, index = np.unique(cradle, return_index = True)#ordino gli angoli cradle in ordine crescente

assert len(cradle) == len(cradle_ordered), "WARNING. THIS PROCEDURE IS NOT GOOD."

ph_ordered = np.array([ph_angles[116][i] for i in index]) #ordino le ph nello stesso ordine di cui sopra, per avere coerenza in un plot!!



fig, ax = plt.subplots()

fig.set_size_inches(7,7)

ax.plot(cradle_ordered, ph_ordered,'--*', color = 'darkblue', label = f'SIPMs average PH as a function \n of the gonio angle\n $\Theta max$ = {cradle_ordered[np.argmax(ph_ordered)]:.2} $\mu rad$ ' )        

ax.set_xlabel(r'$\theta_{cradle}$ ($\mu rad$)', fontsize = 18)

ax.set_ylabel('Mean energy (a.u)', fontsize = 18)

ax.grid()

ax.legend(fontsize = 13, loc = 'lower center')

















