import numpy as np
import h5py  
import matplotlib.pyplot as plt
import copy
import matplotlib as mpl
from matplotlib.colors import ListedColormap
import pandas as pd

#%%Definisco le variabili utili per il futuro dell'analisi.
d=15.9e2 #distanza tra telescopi in cm (15.90 m)
D = (15.9+1.57)*100 #distanza tra 1° tele e sipms in centimetri
D_calo = (15.9+1.57+0.39)*100 #posizione in cm dal primo telescopio al calorimetro

#%% Definisco funzione gaussiana per i prossimi fit.
def Gauss(x, a, mu, sigma):  #gaussiana non normalizzata
    return a * np.exp( (-(x-mu)**2) / (2*sigma**2))

#%% FUNZIONE  GENERICA E FUNZIONALE PER IL CARICAMENTO DATI,

#IN PARTICOLARE VOGLIO DECIDERE MANUALMENTE QUALI VARIABILI SCEGLIERE 

#spiego perchè è necessario costruire le condizioni logiche sulle posizioni xpos:
    #Il produttore mi dice che le camere hanno posizioni comprese tra 0 e 10. Tutti gli altri 
    #eventi sono fallaci. Quindi vanno esclusi. In particolare le posizioni sono 4 -> 
    #x1,y1,x2,y2 (1 = 1° rivelatore 2 = 2°rivelatore). La condizione che decido di costruire
    #è: scarto qualsiasi evento in cui ho ALMENO una posizione bad 

def ImportData( nrun , *info_che_voglio:'str', energy = 0):
    
    """Inserire il numero di run di cui si vogliono importare i dati. Nb vengono implementate le condizioni sulle posizioni (xpos>0 & xpos<10) solo se effettivamente viene chiesto esplicitamente                di caricare 'xpos'.
             Le parole chiave accettate sono: 
              -Ievent
              -digi_base
              -digi_ph
              -digi_time
              -info_plus
              -nclu
              -nstrip
              -xinfo
              -xpos
    """
    

    with h5py.File(f'HDF5/run520{nrun}.h5','r',libver='latest', swmr=True) as hf:
        info_che_voglio = list(info_che_voglio) #passo da tupla a lista perchè potrei dover scambiare di posto delle cose
        
        assert all(isinstance(item, str) for item in info_che_voglio) == True, "WARNING, info_che_voglio MUST BE ALL STR!. \n IF YOURE TRYNG TO DEFINE ENERGY KEEP IN MIND THAT IS A KEY-WORD VARIABLE."#controllo che nella variabile info_che_voglio siano tutte stringhe !!
        
        if 'xpos' in info_che_voglio: #se in info_che_voglio c'è xpos metto 'xpos' come prima della lista !! cosicchè possa creare subito la condizione che xpos>0, xpos<10
            
            info_che_voglio.remove('xpos')
            #metto 'xpos' come primo della lista. Potrebbe sembrare insensato. questa scelta l'ho fatta in vista di un 
            #ottimizzazione del codice. La filosofia è la seguente: E' INUTILE CARICARE PIU VOLTE IN UNA VARIABILE I DATI RELATIVI ALLE POSIZIONI RIVELATE DALLE CAMERE. Infatti in vista di un futuro ciclo di list comprehension dovrò ciclare. 
            #se ciclo in maniera automatica sulle parole chiavi info_che_voglio inevitabilmente mi capiterà di ciclare anche su 'xpos'. PERO io in precedenza voglio creare delle condizioni, dei booleani, che hanno a che fare proprio con le xpos. 
            #Quindi mi pare inutile caricare i dati di xpos, salvarli, creare le condizioni su xpos, e ricaricare i dati di xpos all'interno del ciclo applicandogli la condizione. Quindi quello che penso io qui è, cancello la parola chiave 'xpos',
            # Dopodichè carico i dati delle posizioni, creo le mie condizioni affinche le xpos siano tutte comprese tra 0 e 10, applico un and logico (spiegato dopo xchè). Dopodichè carico tutti i dati che NON siano 'xpos' in un 
            #dizionario (applicando condizione) in maniera ciclata. Alla fine carico i dati di xpos (che erano gia salvati in una variabile) in coda al dizionario. 
        
            
            xpos = np.array(hf["xpos"])
            
            var = (xpos>0)&(xpos<10)
            
            condizione = np.all(var,axis=1) #condizione in AND che tutte e 4 le pos siano comprese tra 0 e 10 (tutti gli eventi con posizione <0 o >10 sono un artefatto (questo me lo dice il produttore dei silici)). L'and logico è fatto in maniera orizzontale rigaxriga. Cioe se almeno una delle 4 posizioni, evento per evento ha un false--> = FALSE (tutto l'evento)
        
            data = {i: np.array(hf[i])[condizione] for i in info_che_voglio} #ciclo su tutti gli elementi, tranni quelli relativi alle posizioni, loro li metto per ultimi, all'ultimo posto
                
            data["xpos"] = xpos[condizione] 
        
            data['energy'] = energy #appendo valori energia
             
        else:  #se xpos non lo voglio, devo necessariamente fare a meno della condizione che sia xpos>0 e <10 (sarebbe un controsenso caricare i dati x fare condizione per poi non salvare dati e condizione.)
            
            data = {i: np.array(hf[i]) for i in info_che_voglio} #carico i dati relativi alle info_che_voglio
             
            data['energy'] = energy #appendo valori energia
                
        return data        
        
#%% Retta di calibrazione 

def Line(x, m, q):
    """Banalissima funzione lineare"""
    return m*x+q


#%%MAPPA DI EFFICIENZA: proviamo a costruire una funzione che costruisca la mappa di efficienza
#nel nostro caso sarà la mappa di efficienza del calorimetro o del SiPM. 

#la mappa di efficienza di un oggetto è definita come il rapporto di due istogrammi 2d
#il rapporto in particolare si farà tra l'istogramma delle posizioni di tutti gli eventi VISTI dall'oggetto 
#e l'isto delle pos degli eventi PASSANTI per l'oggetto. Va da se che il massimo valore del pixel 
# della mappa di efficienza sarà = 1. Altrimenti qualcosa è andato storto. 

#Facendo una divisione di DUE isto 2d è importante che entrambi abbiamo stesso range sia
#in x sia in y e che abbiano lo stesso numero di bins!!


def MappaEff(x1,y1, x2, y2,D:'float', condizione:'list', bins:'int', rang:'list', obj:'str'='non pervenuto'):
    
    """questa funzione prende in pasto gli array di posizione dei telescopi (x1,y1,x2,y2) e la distanza tra il primo telescopio
    e l'oggetto di cui voglio fare la mappa di efficienza (D). Inoltre prende in pasto il numero di bin 
    e il range in cui fare gli isto2d e la mappa efficienza. NB bins e range devono essere uguali per tutti e 3 gli isto 
    sennò le cose non tornano. Di per se la funzione forza queste cose ad essere uguali. obj è una variabile 
    opzionale che serve a stampare nel titolo il nome dell'oggetti di cui si vuole fare la mappa efficienza"""
    
    
    
    #definisco le coordinate proiettive a partire dalle coordinate dei telescopi 
    
    #angoli:
    
    theta_x = np.arctan((x2-x1)/d) #attento!! QUI SI DIVIDE PER d NON D. ovvero per la distanza tra i telescopi!
    
    theta_y = np.arctan((y2-y1)/d)
    
    #calcolo le vere coordinate sul piano dell'oggetto : 
    
    x = D*np.tan(theta_x)
    
    y = D*np.tan(theta_y)
    
    
    #primo istogramma 2d (denominatore della nostra divisione)

    fig,ax=plt.subplots(1,2)
    fig.set_size_inches(25,10)
    fig.subplots_adjust(wspace = .4)
    ax.flatten()
    
    my_cmap = copy.copy(mpl.cm.viridis) # copio cmap di default
    
    zero_color = 'blue'  # Colore per i punti che contano 0
    new_colors = [zero_color] + list(my_cmap.colors[1:])  # Lista dei colori modificati--> con al primo posto il colore che scelgo io! #FACCIO QUESTO LAVORO PERCHE QUANDO USO LA SCALA LOGARITMICA CROMATICA I PUNTI CHE CONTEGGIANO ZERO (o infinito o nan per la mappa di eff (vedi sotto set_bad)) LI VISUALIZZEREI BIANCHI E NON MI PIACE
    my_cmap = ListedColormap(new_colors)

    hnt = ax[0].hist2d(x,y, bins=bins, norm=mpl.colors.LogNorm(),cmap = my_cmap, range=rang ) #salvo le variabili dell'isto 2D perchè mi serviranno dopo per fare la divisione
    ax[0].set_xlabel("x (cm)", fontsize = 25)
    ax[0].set_ylabel("y (cm)", fontsize = 25)
    ax[0].set_title(f" 2D beam profile on the {obj} plane", fontsize = 25)
    
    fig.colorbar(hnt[3], ax=ax[0])
    
    #secondo istogramma 2d (numeratore della nostra divisione)
    
    ht = ax[1].hist2d(x[condizione],y[condizione], bins=bins,norm=mpl.colors.LogNorm(), cmap = my_cmap, range=rang ) #salvo le variabili dell'isto 2D perchè mi serviranno dopo per fare la divisione
    ax[1].set_xlabel("x (cm)", fontsize = 25)
    ax[1].set_ylabel("y (cm)", fontsize = 25)
    ax[1].set_title(f" 2D beam profile on the {obj} plane \n condition -> {obj} sees", fontsize = 25)
    
    fig.colorbar(hnt[3], ax=ax[1])
    
    
    fig,ax=plt.subplots()
    fig.set_size_inches(15,10)
    
    fig.suptitle(f"Efficiency map of {obj}", fontsize = 25)

    
    my_cmap.set_bad(my_cmap(0)) #siccome facendo la divisione di due quantità (ie dei conteggi di un isto 2d, pixel per pixel) potrebbe succedere che divido per zero. Con questo comando

    #setto tutti i punti "bad" (come nan o +- inf) con il 1° colore della mia colormap!
    divisione = ht[0]/hnt[0] #faccio la divisione pixel per pixel--> Attento questa è la trasposta di quello che visualizzo!! --> quindi dovrò plottare divisione.T
    
    myPlotEff = ax.imshow(divisione.T, norm=mpl.colors.LogNorm(), cmap = my_cmap, origin = "lower",
                       extent = (ht[1].min(), ht[1].max(), ht[2].min(), ht[2].max())) #estendo l'immagine (extent) dal minimo dell'asse x dell'isto 2d numeratore al massimo dello stesso. Idem per y.
    ax.set_xlabel('x (cm)', fontsize = 25)
    ax.set_ylabel('y (cm)', fontsize = 25)
    fig.colorbar(myPlotEff, ax = ax)
    
    #righe di controllo
    
    cond = divisione >1 # cerco se almeno un pixel sia maggiore di 1 la qual cosa non ha senso, non posso avere più eventi visti dall'oggetto rispetto quelli passati dall'oggetto.
    assert cond.any() == False, "Something goes wrong into division! Fix it before going on " #Se c'è ALMENO un valore >1 nella matrice divisione allora avrò cond.any() == True e il codice mi avvertirà dell'errore

#%% Funzione per passare da ADC ad energia

def adc2en(adc:'np.ndarray', m, q):
    """ inserire i dati in adc, e i parametri della retta di calibrazione (m, q)"""

    return  (adc-q)/m
    
#%% FUNZIONE CHE MI METTE IN ORDINE UNA MATRICE BIDIMENSIONALE.

def unique_columns_with_indices(matrix):
    
    """Matrice che mi mette in ordine una matrice bidimensionale (2 colonne, tante righe).
    Tale funzione mette in ordine, senza ripetizione prima rispetto la prima colonna, poi rispetto la seconda colonna.
    Inoltre restituisce anche gli indici di riga a cui si trovano le coppie di elementi, sempre in ordine crescente rispetto la prima colonna e poi la seconda colonna.
    UTILIZZO DI DATAFRAME, per comodità"""
    df = pd.DataFrame(matrix, columns=['col1', 'col2'])#Viene creato un DataFrame df utilizzando Pandas, in cui le colonne sono etichettate come "col1" e "col2" per rappresentare le due colonne della matrice.
    sorted_df = df.drop_duplicates(subset=['col1', 'col2']).sort_values(by=['col1', 'col2']) #Viene utilizzato il metodo drop_duplicates su df per rimuovere le righe duplicate basate sulle colonne "col1" e "col2". Questo ci fornisce i valori unici delle colonne nella matrice.
    unique_rows = sorted_df.values #estraggo i valori unici delle due colonne                                                              #inoltre utilizziamo il metodo sort_values su df per ordinare il DataFrame in base alle colonne "col1" e "col2", ottenendo così le colonne uniche in ordine crescente. Il che vuol dire, ordina le coppie secondo la prima colonna, all'interno di questo subset trova i valori della seconda colonna + piccoli.
    unique_indices = [df.index[(df['col1'] == row[0]) & (df['col2'] == row[1])].tolist() for row in unique_rows]#trovo gli indici in cui df ha la colonna1 = alla prima colonna della matrice formata da coppie uniche e colonna 2 = alla seconda colonna della matrice composta da coppie uniche. Infine tieni a mente che mi vengono restituiti gli indici in ordine! ie in ordine di pandas, ovvero ordinando prima la prima colonna, poi la seconda! proprio quello che serve a me.
    #inoltre gli indici che mi ritornano sono indici di riga. Sotto un piccolo esempio
    
    return unique_rows, unique_indices



# Esempio di utilizzo, penso che sia utile averlo qui sempre :) 
#matrice = np.array([[0, 1], [0, 1], [0, 1],[0,2],
                    #[2, 1], [2, 1], [1, 2], [1, 2]])
#colonne_uniche, indici_ordinati = unique_columns_with_indices(matrice)

#print("Colonne uniche in ordine crescente:")
#print(colonne_uniche)
#print("Indici corrispondenti:")
#for row, indices in zip(colonne_uniche, indici_ordinati):
    #print(f"{row}: {indices}")


