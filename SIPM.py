# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 16:48:11 2023

Vediamo ora i dati provenienti dai SIPM. Le matrici che vedono qualche segnale saranno essenzialmente
la matrice 2 e 3 (canali 1 2 digitizer).
quello che vogliamo fare è capire se si riesce a calibrare il SIPM ovvero se si riesce 
a calcolare l'energia rilasciata all'interno dei sipm, dopo al cristallo. L'idea è quella di 
fittare il picco gaussiano della matrice colpita, e fare la differenza con l'energia misurata a valle
dell'esperimento. Preannunciamo che sarà molto difficile ottenere risultati attendibili in quanto
il fascio quando incontra i cristalli inizia a sciamare, dunque molte particelle avranno 
angoli non trascurabili e quindi potranno mancare il calorimetro.  '

@author: Leo
"""
#%% Cose preliminari 

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


from funzioni_utili import ImportData
from funzioni_utili import Gauss
from funzioni_utili import adc2en
from funzioni_utili import MappaEff
from funzioni_utili import D


import json


#%% la run che andiamo ad indagare è la 155--> importiamo i dati delle ph e delle xpos delle 
#camere

data = ImportData(155,'xpos', 'digi_ph',energy = 120)

#iniziamo a dare uno sguardo ai dati, vediamo se è vero che il fascio è centrato sulle matrici 2+3



#%% Iniziamo a vedere quale matrice vede segnale 


matrix = np.array([2,3,1,4]) #indicizzano (in ordine di canale del digi) il numero di matrice

fig1, ax1 = plt.subplots(2,2)

fig1.set_size_inches(15,10)

ax1 = ax1.flatten()


#creo piu immagini, in una plotto gli isto di conteggi, nelle altre le proiezioni delle posizioni sul piano dei sipm e  la mappa di efficienza delle 4 diverse matrici di sipm (vedi documentazione mappaeff--> funzioni_utili/help.)
#creo la mappa di efficienza dei sipm per controllare se per caso il fascio colpisce il bordo del sipm/cristallo. In caso fosse così potrei avere delle perdite di energia perchè significa che non sono centrato.

for i,mat in enumerate(matrix):
    
    h, bins = np.histogram(data['digi_ph'][:,i+1], bins = 100) #nb i+1 perchè i canali dei sipm partono da 1 
    
    binc = bins[:-1]+(bins[1]-bins[0])/2
    
    ax1[i].plot(binc, h, label = f'Counts histogram matrix {mat}',ds = 'steps-mid', color = 'darkgreen')
    
    ax1[i].set_xlabel('Energy (ADC)', fontsize = 15)
    
    ax1[i].set_xlabel('Counts', fontsize = 15)
    
    ax1[i].legend(fontsize = 15)

    ax1[i].grid()
    
    #mappa di efficienza
    #la condizione che impongo per stabilire se l'evento è passato effettivamente dal sipm è
    #ph_sipm>500.
    
    #se vedessi un bordo netto significherebbe che tutti gli eventi visti dai sipm (condizione>500) si interrompono di botto tutti ad una stessa x per esempio.
    #quindi questo mi direbbe che il numeratore del rapporto della mappa di efficienza, ovvero appunto l'isto2d delle posizioni CON CONDIZIONE che il sipm veda si interrompe ad una determinata X per tutti gli eventi. 
    #Vuol dire che il sipm da quella x in poi NON è in grado di vedere niente--> vuol dire che il fascio impatta sul bordo del cristallo e dal bordo in poi non ho piu segnale nei SIPM!!

    
    cond = data['digi_ph'][:,i+1]>500 
    
    MappaEff(data['xpos'][:,0] ,data['xpos'][:,1] ,data['xpos'][:,2], data['xpos'][:,3] ,D,cond, 100, [[-1,1],[-1,1]], f'Sipm {mat}' )
    
    #Non vediamo bordi netti quindi siamo sicuri che il fascio è contenuto su tutta la superficie del 
    #cristallo. Il fascio inoltre lascia segnale solo in mat2 e mat3. negli altri canali (confermato anche dalla mappa di efficienza) non vediamo niente se non il pedestallo
    
    
#%% ORA VEDIAMO COSA VEDONO I CALORIMETRI IN FONDO A TUTTO 

#importo i valori di eta

# Apro il file JSON in modalità lettura
with open('DATA/eta.json', 'r') as f:
    # Carica il contenuto del file JSON in un dizionario
    eta = json.load(f)

print(eta.items()) #per vedere che sia andato tutto per il verso giusto 


calibrazione = np.load('DATA/parametri_calibrazione_tot.npz')

popt_calibrazione = calibrazione['popt_L'] #m e q della retta di calibrazione

#RICORDIAMO ANCHE QUI CHE L'ORDINE DELLE CHIAVI E' IN ORDINE DEL CANALE DEL DIGI, PER SEMPLIFICARCI LA VITA, IN CASO SERVISSE
#IE CANALE 8 = LGBL 9=LGBR EC..

#equalizzo le ph: 
    
ph_equa = {key: data['digi_ph'][:,i+8]/eta[key] for i,key in enumerate(eta.keys())} #RICORDA QUESTO LO PUOI FARE PERCHE LE CHIAVI DEI CALO ('LGBL' ECC ) SONO IN ORDINE DI COMPARIZIONE NEI CANALI DEL DIGI. Ricorda i calorimetri vanno dall'8 al 14 (canale)
#conservo queste in un dizionario in caso mi servissero separate... potrei non averne bisogno ma vabhe, dipende come evolve l'analisi. 
    
#sommo tutte le ph equalizzate, evento per evento.

ph_equa_sum = sum(ph_equa.values())

#adesso che ho equalizzato le ph posso passare da adc a gev tramite adc2en:
    
ph_gev = adc2en(ph_equa_sum, *popt_calibrazione)


#adesso vediamo l'istogramma dentro al calo(in gev)

h, bins = np.histogram(ph_gev, bins = 200, range = [50,150])

binc = bins[:-1]+(bins[1]-bins[0])/2

fig, ax = plt.subplots()

fig.set_size_inches(15,10)

ax.plot(binc, h, label ='Calorimeters histogram of counts', ds ='steps-mid',color = 'darkgreen')

ax.grid()

ax.set_xlabel('Energy (ADC)', fontsize = 15)

ax.set_ylabel('counts', fontsize = 15)


#cerco di definire in maniera automatica una zona intorno al centro del picco dello spettro 
#siccome è un intervallo asimmetrico rispetto al centro, dato che il beam è sporco ad energie 
#basse devo fare delle condizioni asimmetriche 

cond_destra = (binc>binc[np.argmax(h)]) & (h>0.1*np.max(h)) #condizione di essere a destra del picco & essere maggiore del 10 percento del massimo del picco. 

destra = np.max(binc[cond_destra]) #trovo il valore della x (binc) della linea verticale di destra


cond_sinistra = (binc<binc[np.argmax(h)]) & (h>0.75*np.max(h))#condizione di essere a sinistra del picco & essere maggiore  del 70 percento del massimo del picco.

sinistra = np.min(binc[cond_sinistra]) #trovo il valore della x (binc) della linea verticale sinistra da plottare


cond2fit = (binc>sinistra) & (binc<destra) #condizione del range per fittare

p0 = [np.max(h), binc[np.argmax(h)], (destra-binc[np.argmax(h)])]

popt, pcov = curve_fit(Gauss, binc[cond2fit], h[cond2fit], p0 = p0, 
                       sigma=np.sqrt(h[cond2fit]),absolute_sigma=True)

ax.plot(binc[cond2fit], Gauss(binc[cond2fit], *popt), ls ='--', lw=4, label =f'fitted gaussian \n $ \mu $ = {popt[1]:.3} $\pm$ {np.sqrt(pcov[1,1]):.3}\n $\sigma$ = {popt[2]:.3} $\pm$ {np.sqrt(pcov[2,2]):.3}', color ='black')

ax.legend(fontsize = 16.5, loc = 'lower center')

ax.set_yscale('log')

print(f"Sembrerebbe che nel SiPM vengano rilasciati {120-popt[1] :.3} GeV. \n Questo non rispecchia la realtà fisica, simulazioni e calcoli ci dicono che sono troppi. Dobbiamo rinunciare alla calibrazione del SiPM")

#17GeV all'interno di 4X0 di cristallo sono davvero troppi. Quello che probabilmente succede è che 
#il fascio iniziando a fare sciame elettromagnetico si apre molto in angolazione (questa angolazione noi non ce l'abbiamo purtroppo, i.e quella dello sciame. Abbiamo solo quella dell'apertura del beam prima sulle camere, ma le camere sono all'inizio di tutta la catena sperimentale :( ))
#Quindi quello che succede è che buona parte dell'energia missing (17GeV) manchi i calorimetri e quindi non viene registrata.
#Quindi inevitabilmente NON possiamo calibrare i SiPM. Tutte le considerazioni a seguire saranno, purtroppo, fatte tutte in ADC. 


    
    
    

